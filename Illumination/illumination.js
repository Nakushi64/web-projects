////////////////////////////////////////////////////////////////////////////////
// illumination.js
//
// Bearbeiten Sie diese Datei fuer den Praktikumsteil "Illumination".
//
// HS Duesseldorf - Fachbereich Medien - Grundlagen d. Computergrafik
//
// Studiengang:
// Gruppe     : L 
// Autor 1    : Emre Yalcinkaya
// Autor 2    : Till von der Lippe
// Autor 3    : Yazan Al Shoufani
// Autor 4    : Julia Stieler
// Autor 5    : Lukas Scheunert
// Autor 6	  : Max Ostermeier
////////////////////////////////////////////////////////////////////////////////



// das ambiente Licht
var ambientLight = {intensity: {r: 0.125, g: 0.125, b: 0.125}};

// alle übrigen (Punkt)-Lichter der Szene
var lights = [
   {position: new THREE.Vector3(-100, 100, 75),
   intensity: {r: 0.875, g: 0.625, b: 0.125}},
   {position: new THREE.Vector3(100, -75, 75),
   //intensity: {r: 0.0, g: 0.0, b: 0.0}},
   intensity: {r: 0.825, g: 0.0625, b: 0.0875}},
   {position: new THREE.Vector3(50, 100, -100),
    //intensity: {r: 0.0, g: 0.0, b: 0.0}},
    intensity: {r: 0.05, g: 0.05, b: 0.9}},
];

//Koeffizienten
var material = {r: 0.1, g: 0.5, b:0.5};
var difCof   = {r:0.5 ,g:0.5,b:0.5};
var specCof  = {r:1,g:1,b:1};

////////////////////////////////////////////////////////////////////////////////
// initScene()
// Initialisierung.
// Wird automatisch beim Start aufgerufen. 
////////////////////////////////////////////////////////////////////////////////
function initScene()
{
  registerLights(lights);
}

//Funktion zur Normalisierung der Vektoren
function normieren(xPos, yPos, zPos){
	var norm = Math.sqrt(Math.pow(xPos,2)+ Math.pow(yPos,2)+Math.pow(zPos,2));
	xPos = xPos/norm;
	yPos = yPos/norm;
	zPos = zPos/norm;
	var vectornormi ={x:xPos,y:yPos,z:zPos};
	return vectornormi;
}

////////////////////////////////////////////////////////////////////////////////
// phong(position, normal, camPosition)
// Wird während des Renderings für jeden Vertex einmal aufgerufen.
// Parameter: position     Vertexposition (Vector3)
//            normal       Vertexnormale (Vector3)
//            camPosition  Kameraposition (Vector3)
// Rueckgabe: Eine Farbe. D.h. ein Objekt mit den Attributen .r, .g und .b
//
// Hinweis: Alle Parameter befinden sich im selben Koordinatensystem.
////////////////////////////////////////////////////////////////////////////////
function phong(position, normal, camPosition)
{
	
	 // Initialisiere den Rueckgabewert
	 var outColor = {r: 0.0, g: 0.0, b: 0.0};
	 
	 //Bonus++Animation+++++++++++++++++++++++++++++++++++++++++
	 var winkel = Math.PI/16;
	 lights[0].position = new THREE.Vector3(lights[0].position.x,lights[0].position.y*Math.cos(winkel)+lights[0].position.z*(-Math.sin(winkel)),lights[0].position.y*Math.sin(winkel)+lights[0].position.z*Math.cos(winkel));
	 lights[1].position = new THREE.Vector3(lights[1].position.x*Math.cos(winkel)+lights[1].position.z*Math.sin(winkel),lights[1].position.y,lights[1].position.x*(-Math.sin(winkel))+lights[1].position.z*Math.cos(winkel));
	 lights[2].position = new THREE.Vector3(lights[2].position.x*Math.cos(winkel)+lights[2].position.y*(-Math.sin(winkel)),lights[2].position.x*Math.sin(winkel)+lights[2].position.y*Math.cos(winkel),lights[2].position.z);
	 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 
	 //Normalisierte Vectoren
	 var normiertL = [];
	 for (i=0;i<lights.length;i++){
		 normiertL.push(normieren(lights[i].position.x,lights[i].position.y,lights[i].position.z));
	 }
	 var normiertN = normieren(normal.x,normal.y,normal.z);
	
	 
	 // TODO: Implementieren Sie die Beleuchtungsberechnung
	 //       mit dem Phong-Beleuchtungsmodell.
	 var iGes = ambienterLichtAnteil();
	 for (i=0;i<lights.length;i++){
		 //Skalarprodukt N*L
		 var NL = normiertL[i].x*normiertN.x+normiertL[i].y*normiertN.y+normiertL[i].z*normiertN.z;
		 var iDif = diffuserLichtAnteil(normal,normiertL[i], normiertN, NL);
		 var iSpec = spekularerLichtAnteil(camPosition,normiertL[i],normiertN,NL);
		 outColor = {r: outColor.r+lights[i].intensity.r*(iDif.r+iSpec.r), g: outColor.g+lights[i].intensity.g*(iDif.g+iSpec.g), b: outColor.b+lights[i].intensity.b*(iDif.b+iSpec.b)};
	 }
	  outColor = {r: outColor.r+iGes.r, g:outColor.g+iGes.g, b:outColor.b+iGes.b};
	 // console.log(outColor);
	 // Rueckgabe des berechneten Farbwerts
	 return outColor;
}

//Funktionen zur Berechnung der Lichtanteile------------------------------------------------
function ambienterLichtAnteil(){
	 var iGes = {r: material.r*ambientLight.intensity.r, g:material.g*ambientLight.intensity.g, b: material.b*ambientLight.intensity.b};
	 return iGes;
}

function diffuserLichtAnteil(normal, normiertL, normiertN, NL){
	 if(NL <=0)
	 {
		NL=0;
	 }
	 var iDif = {r: difCof.r*NL, g: difCof.g*NL,b: difCof.b*NL};
	 return iDif;
}

function spekularerLichtAnteil(camPos,normiertL, normiertN, NL){
	 var R = {x:2*NL*normiertN.x-normiertL.x, y:2*NL*normiertN.y-normiertL.y,z:2*NL*normiertN.z-normiertL.z} ;
	 var V = normieren(camPos.x,camPos.y,camPos.z);
	 var n = 9;
	 var VR = Math.pow(R.x*V.x+R.y*V.y+R.z*V.z,n);
	 if(VR<=0){
		VR =0;
	 }
	 var iSpec ={r:specCof.r*VR, g:specCof.g*VR,b:specCof.b*VR};
	 return iSpec;
}
//-----------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
// bresenham.js
//
// Bearbeiten Sie diese Datei für den Praktikumsteil "Bresenham Line".
//
// HS Duesseldorf - Fachbereich Medien - Grundlagen d. Computergrafik
//
// Studiengang:
// Gruppe     :
// Autor 1    :
// Autor 2    :
// Autor 3    :
// Autor 4    :
// Autor 5    :
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// drawLine(x0, y0, x1, y1)
// Diese Funktion soll eine Linie von (x0, y0) nach (x1, y1) zeichnen.
// Implementieren Sie dazu den Bresenham-Line-Algorithmus für alle Oktanten
// in dieser Funktion. Einen Punkt zeichnen Sie mit setPixel(x,y).
////////////////////////////////////////////////////////////////////////////////

var Pos = [0,0];
var movRight = true;
var movDown = true;
var lastFrame = 0;
var FPS = 48;
var gx0,gx1,gy0,gy1,gm;
var activateWishy = true;

function drawLine(x0, y0, x1, y1){
	gx0=x0;
	gx1=x1;
	gy0=y0;
	gy1=y1;
	
	var dX = x1 - x0;
	var dY = y1 - y0;
	var m = dY/dX;
	var drawY = y0;
	var drawX = x0;
	var sumSteigung = 0;
	console.log("M: "+m);
	//Oktanten x>=0
	if(m<=1 && m>=-1){
		if(dX >= 0){
				for( i = x0; i <= x1; i++ ){
					sumSteigung = sumSteigung+m;
					//console.log("SumSteigung: "+sumSteigung);
					//Oktant 1
					if(sumSteigung>1){
						drawY++;
						sumSteigung--;
					}
					//Oktant 8
					else if(sumSteigung <-1){
						drawY--;
						sumSteigung++;
					}
					setPixel(i,drawY);
				}
		}
		//Oktanten x<0
		else {
			for( i = x0; i >= x1; i-- ){
				sumSteigung = sumSteigung+m;
				//console.log("SumSteigung: "+sumSteigung);
				//Oktant 4
				if(sumSteigung>1){
					drawY--;
					sumSteigung--;
				}
				//Oktant 5
				else if(sumSteigung <-1){
					drawY++;
					sumSteigung++;
				}
				setPixel(i,drawY);
			}
		}
	}
	else{
		m=dX/dY;
		if(dY >= 0){
				for( i = y0; i <= y1; i++ ){
					sumSteigung = sumSteigung+m;
					//console.log("SumSteigung: "+sumSteigung);
					//Oktant 3
					if(sumSteigung>1){
						drawX++;
						sumSteigung--;
					}
					//Oktant 2
					else if(sumSteigung <-1){
						drawX--;
						sumSteigung++;
					}
					setPixel(drawX,i);
				}
		}
		//Oktanten x<0
		else {
			for( i = y0; i >= y1; i-- ){
				sumSteigung = sumSteigung+m;
				//console.log("SumSteigung: "+sumSteigung);
				//Oktant 6
				if(sumSteigung>1){
					drawX--;
					sumSteigung--;
				}
				//Oktant 7
				else if(sumSteigung <-1){
					drawX++;
					sumSteigung++;
				}
				setPixel(drawX,i);
			}
		}
	}
}

function Wishy(timestamp){
	if(timestamp < lastFrame+(1000/FPS)){
	requestAnimationFrame(Wishy);
	return;
	}
	lastFrame = timestamp;
	if(movRight){Pos[0]++;}
	else{Pos[0]--;}
	if(movDown){Pos[1]++;}
	else{Pos[1]--;}
	if(Pos[1]>=240){movDown=false;}
	if(Pos[1]<=0){movDown=true;}
	if(Pos[0]>=320){movRight=false;}
	if(Pos[0]<=0){movRight=true;}
	if(Collision()){
		
		if(gm<=0.5 && gm>=-0.5){
			movDown = !movDown;
		}
		else if(gm<=-2 || gm>=2){
			movRight = !movRight;
		}
		else{
		movRight = !movRight;
		movDown = !movDown;
		}
	}
	setPixel(Pos[0],Pos[1]);
	console.log(Pos[0]+" "+Pos[1]);
	if(activateWishy){requestAnimationFrame(Wishy);}
}

function Collision(){
	var dX = gx1 - gx0;
	var dY = gy1 - gy0;
	var m = dY/dX;
	gm = m;
	var drawY = gy0;
	var drawX = gx0;
	var sumSteigung = 0;
	if(m<=1 && m>=-1){
		if(dX >= 0){
				for( i = gx0; i <= gx1; i++ ){
					sumSteigung = sumSteigung+m;
					//console.log("SumSteigung: "+sumSteigung);
					//Oktant 1
					if(sumSteigung>1){
						drawY++;
						sumSteigung--;
					}
					//Oktant 8
					else if(sumSteigung <-1){
						drawY--;
						sumSteigung++;
					}
					if( Pos[0] == i-1 && Pos[1] ==drawY-1||
						Pos[0] == i && Pos[1] ==drawY-1||
						Pos[0] == i+1 && Pos[1] ==drawY-1||
						Pos[0] == i-1 && Pos[1] ==drawY||
						Pos[0] == i && Pos[1] ==drawY||
						Pos[0] == i+1 && Pos[1] ==drawY||
						Pos[0] == i-1 && Pos[1] ==drawY+1||
						Pos[0] == i && Pos[1] ==drawY+1||
						Pos[0] == i+1 && Pos[1] == drawY+1){return true;}
				}
		}
		//Oktanten x<0
		else {
			for( i = gx0; i >= gx1; i-- ){
				sumSteigung = sumSteigung+m;
				//console.log("SumSteigung: "+sumSteigung);
				//Oktant 4
				if(sumSteigung>1){
					drawY--;
					sumSteigung--;
				}
				//Oktant 5
				else if(sumSteigung <-1){
					drawY++;
					sumSteigung++;
				}
				if(     Pos[0] == i-1 && Pos[1] ==drawY-1||
						Pos[0] == i && Pos[1] ==drawY-1||
						Pos[0] == i+1 && Pos[1] ==drawY-1||
						Pos[0] == i-1 && Pos[1] ==drawY||
						Pos[0] == i && Pos[1] ==drawY||
						Pos[0] == i+1 && Pos[1] ==drawY||
						Pos[0] == i-1 && Pos[1] ==drawY+1||
						Pos[0] == i && Pos[1] ==drawY+1||
						Pos[0] == i+1 && Pos[1] == drawY+1){return true;}
			}
		}
	}
	else{
		m=dX/dY;
		if(dY >= 0){
				for( i = gy0; i <= gy1; i++ ){
					sumSteigung = sumSteigung+m;
					//console.log("SumSteigung: "+sumSteigung);
					//Oktant 3
					if(sumSteigung>1){
						drawX++;
						sumSteigung--;
					}
					//Oktant 2
					else if(sumSteigung <-1){
						drawX--;
						sumSteigung++;
					}
					if( Pos[0] == drawX-1 && Pos[1] ==i-1||
						Pos[0] == drawX && Pos[1] ==i-1||
						Pos[0] == drawX+1 && Pos[1] ==i-1||
						Pos[0] == drawX-1 && Pos[1] ==i||
						Pos[0] == drawX && Pos[1] ==i||
						Pos[0] == drawX+1 && Pos[1] ==i||
						Pos[0] == drawX-1 && Pos[1] ==i+1||
						Pos[0] == drawX && Pos[1] ==i+1||
						Pos[0] == drawX+1 && Pos[1] == i+1){return true;}
				}
		}
		//Oktanten x<0
		else {
			for( i = gy0; i >= gy1; i-- ){
				sumSteigung = sumSteigung+m;
				//console.log("SumSteigung: "+sumSteigung);
				//Oktant 6
				if(sumSteigung>1){
					drawX--;
					sumSteigung--;
				}
				//Oktant 7
				else if(sumSteigung <-1){
					drawX++;
					sumSteigung++;
				}
				if( Pos[0] == drawX-1 && Pos[1] ==i-1||
						Pos[0] == drawX && Pos[1] ==i-1||
						Pos[0] == drawX+1 && Pos[1] ==i-1||
						Pos[0] == drawX-1 && Pos[1] ==i||
						Pos[0] == drawX && Pos[1] ==i||
						Pos[0] == drawX+1 && Pos[1] ==i||
						Pos[0] == drawX-1 && Pos[1] ==i+1||
						Pos[0] == drawX && Pos[1] ==i+1||
						Pos[0] == drawX+1 && Pos[1] == i+1){return true;}
			}
		}
	}
}
////////////////////////////////////////////////////////////////////////////////
// example(i)
// Diese Funktion dient als Codebeispiel.
// Sie wird beim Laden der Seite aufgerufen und kann entfernt werden.
////////////////////////////////////////////////////////////////////////////////
function example(i)
{
  var y = i + 2;
  for (var x = 0; x < 400; x++)
  {
    y--;
    if (y < -i)
    {
      y = i;
    }
    setPixel(x, Math.abs(y));
  }
}
